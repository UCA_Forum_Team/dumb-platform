import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navigation from './components/Navbar/Navigation';
import AboutUs from './components/AboutUs/AboutUs';
import Footer from './components/Footer/Footer';
import Home from "./components/Home/Home";
import Courses from './components/Courses/Courses';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import Mentoring from './components/Mentoring/Mentoring'
import Articles from './components/Articles/Articles'
import Dashboard from './components/Dashboard/Dashboard'
import AdminDashboard from './components/Admin/AdminDashboard'

class Guest extends Component {
  render() {
    return (
      <React.Fragment>

        
        <Router>
        <Navigation />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/Aboutus" component={AboutUs} />
            <Route path="/Courses" component={Courses} />
            <Route path="/Articles" component={Articles} />
            <Route path="/Mentoring" component={Mentoring} />
            <Route path="/Login" component={Login} />
            <Route path="/Register" component={Register} />
          </Switch>
          <Footer />
        </Router>
        
      </React.Fragment>
    )
  }
}

class App extends Component {
  render() {
    return (
      <React.Fragment>
        
          
          <Router>
            <Switch>
              <Route path="/Dashboard/" component={Dashboard} />
              <Route path="/Admin/" component={AdminDashboard} />
              <Route component={Guest} />
            </Switch>
          </Router>

      </React.Fragment>
    );
  }
}


export default App;