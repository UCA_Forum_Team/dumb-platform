import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Post from './Post'
import Posts from './Posts'

class Articles extends Component {
  render() {
    return (
      <React.Fragment>

        
        <Router>
          <Switch>
            <Route exact path="/Articles" component={Posts} />
            <Route path="/Articles/post/:id" component={Post} />
          </Switch>
        </Router>
        
      </React.Fragment>
    )
  }
}
export default Articles;