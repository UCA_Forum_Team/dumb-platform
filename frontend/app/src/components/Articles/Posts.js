import React, { Component } from "react";
import "./articles.css";
import {
  Link
} from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import { Card, Button, Modal } from "react-bootstrap";

export default class Posts extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.state = {
      show: false,
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <React.Fragment>
        <Container className="course">
          <Row style={{ textAlign: "center" }}>
            <div className="container">
              <div className="row">
                {/* Blog Entries Column */}
                <div className="col-md-8">
                  <h1 className="my-4">Articles
                  </h1>
                  {/* Blog Post */}
                  <div className="card mb-4">
                    <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
                    <div className="card-body">
                      <h2 className="card-title">Post Title</h2>
                      <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                      <Link className="btn btn-primary" to="Articles/post/1" >Read More →</Link>
                      
                    </div>
                    <div className="card-footer text-muted">
                      Posted on January 1, 2020 by
                      <a href="#">Start Bootstrap</a>
                    </div>
                  </div>
                  {/* Blog Post */}
                  <div className="card mb-4">
                    <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
                    <div className="card-body">
                      <h2 className="card-title">Post Title</h2>
                      <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                      <a href="#" className="btn btn-primary">Read More →</a>
                    </div>
                    <div className="card-footer text-muted">
                      Posted on January 1, 2020 by
                      <a href="#">Start Bootstrap</a>
                    </div>
                  </div>
                  {/* Blog Post */}
                  <div className="card mb-4">
                    <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
                    <div className="card-body">
                      <h2 className="card-title">Post Title</h2>
                      <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                      <a href="#" className="btn btn-primary">Read More →</a>
                    </div>
                    <div className="card-footer text-muted">
                      Posted on January 1, 2020 by
                <a href="#">Start Bootstrap</a>
                    </div>
                  </div>
                  {/* Pagination */}
                  <ul className="pagination justify-content-center mb-4">
                    <li className="page-item">
                      <a className="page-link" href="#">← Older</a>
                    </li>
                    <li className="page-item disabled">
                      <a className="page-link" href="#">Newer →</a>
                    </li>
                  </ul>
                </div>
                {/* Sidebar Widgets Column */}
                <div className="col-md-4">
                  {/* Search Widget */}
                  <div className="card my-4">
                    <h5 className="card-header">Search</h5>
                    <div className="card-body">
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Search for..." />
                        <span className="input-group-append">
                          <button className="btn btn-secondary" type="button">Go!</button>
                        </span>
                      </div>
                    </div>
                  </div>
                  {/* Categories Widget */}
                  <div className="card my-4">
                    <h5 className="card-header">Categories</h5>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-lg-6">
                          <ul className="list-unstyled mb-0">
                            <li>
                              <a href="#">Web Design</a>
                            </li>
                            <li>
                              <a href="#">HTML</a>
                            </li>
                            <li>
                              <a href="#">Freebies</a>
                            </li>
                          </ul>
                        </div>
                        <div className="col-lg-6">
                          <ul className="list-unstyled mb-0">
                            <li>
                              <a href="#">JavaScript</a>
                            </li>
                            <li>
                              <a href="#">CSS</a>
                            </li>
                            <li>
                              <a href="#">Tutorials</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Side Widget */}
                  <div className="card my-4">
                    <h5 className="card-header">Side Widget</h5>
                    <div className="card-body">
                      You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
              </div>
                  </div>
                </div>
              </div>
              {/* /.row */}
            </div>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}
