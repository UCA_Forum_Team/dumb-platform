import React , {Component} from 'react'
import './login.css'
import {Nav, Image} from 'react-bootstrap'
import Img from "./img1.png"
import Form from "./Form"
import axios from 'axios'


class logIn extends Component{
    constructor(){
        super()
        this.state = {
            person: {}
        }

    }
    // componentDidMount(){
    //     fetch("http://localhost:8000/api/v1/users/get_users/")
    //     .then (res=> res.json())
    //     .then(res=>{
    //         console.log(res)
    //         this.setState({person: res[0]})
    //         console.log(this.state.person)
    //     })
    // }
    render() {
        return(
            <div className='container split'>
                <div className='box1 left'>
                    <div className='centerL' id='left'>
                        <div>
                            <p style={{fontSize: "30px", fontWeight:"large"}}>
                                Good to see you're back!
                            </p>
                            <p style={{fontSize: "20px"}}>
                                Login to enter your study mode
                            </p>
                        </div>
                        <div style={{color:"blueviolet"}}>
                        <Form/>
                        </div>
                        <div>
                            <p style={{display:"flex", alignItems:"baseline"}}>
                                Don't have an account?
                                <Nav.Link href="Register">Sign up</Nav.Link>                                
                            </p>
                            <a href="forgotPassword">Forget</a> 
                        </div>
                    </div>
                </div>
                <div className='box2 right'>
                    <div class='centerR' id="right">
                        <Image src= {Img} alt="photo"style={{maxWidth:"100%"}}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default logIn;
