import React, { Component } from 'react';
import Axios from "axios";


const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach(
    (val) => val.length > 0 && (valid = false)
  );
  return valid;
}

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: null,
      email: null,
      password: null,
      response: '',
      errors: {
        fullName: '',
        email: '',
        password: '',
      }
    };
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'email':
        errors.email =
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
      case 'password':
        errors.password =
          value.length < 3
            ? 'Password must be 8 characters long!'
            : '';
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  }

  handleSubmit = (event) => {
    this.setState({ isLogining: true })
    event.preventDefault();
    console.log(validateForm(this.state.errors))
    if (validateForm(this.state.errors)) {
      Axios.post("http://127.0.0.1:8000/api/v1/users/login/", {
        email: "admin@gmail.com",
        password: "admin"
      })
        .then(response => {
          this.setState({ isLogining: false })
          if (response.data.success) {
            localStorage.setItem('userToken', response.data.token);
            localStorage.setItem('role', response.data.position);
            if (response.data.position != 'admin') {
              window.location.href = '/dashboard'
            } else {
              window.location.href = '/admin'
            }

          }
        })
        .catch(error => {
          this.setState({ isLogining: false })
          if (error.response) {
            if (error.response.data.non_field_errors[0]) {
              alert(error.response.data.non_field_errors[0])
            }
          } else {
            alert("Problems with server, please contact us")
          }

        });
    } else {
      console.error('Invalid Form')
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className='wrapper'>
        <div className='form-wrapper'>
          <form onSubmit={this.handleSubmit} noValidate>
            <div className='email'>
              <input
                type='email'
                name='email'
                onChange={this.handleChange}
                placeholder='Email'
                noValidate /><br />

              {errors.email.length > 0 &&
                <span className='error'>{errors.email}</span>}
            </div>
            <div className='password'>
              <input
                type='password'
                name='password'
                onChange={this.handleChange}
                placeholder='Password'
                noValidate /><br />

              {errors.password.length > 0 &&
                <span className='error'>{errors.password}</span>}
            </div>
            <div className='submit' onClick={this.handleSubmit}>
              {
                this.setState.isLogining ? 'Logining' : <button >Login</button>
              }

            </div>
          </form>
        </div>
      </div>
    );
  }
}


export default Register



