import React, { Component } from "react";
import "./register.css";
import { Container, Row, Col } from "reactstrap";
import { Image, Form, Button } from "react-bootstrap";
import pic1 from "./img1.png";
import RegForm from "./Form";



export default class Register extends Component {
  render() {
    return (
      <Container className="register">
        <Row>
          <Col
            xs={12}
            md={6}
            style={{ textAlign: "center"}}
            className="d-none d-lg-block"
          >
            <Image
              style={{ marginTop: "10%" }}
              src={pic1}
              height="400"
              alt="Picture"
            />
          </Col>
          <Col xs={12} md={6}>
            <h3>Get started for free!</h3>
            <h5>Signup now and avial our services</h5>
            <RegForm/>
            <br />
            <p>
              Already have an accout? <a href="Login">Login</a>
            </p>

            <p>
              Creating an account means you agree to our{" "}
              <a href="#">Terms & Conditions</a> and
              <a href="#"> Privacy Policy</a>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}
