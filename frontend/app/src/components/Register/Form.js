import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";


const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach(
    (val) => val.length > 0 && (valid = false)
  );
  return valid;
}

const check = (pass1, pass2) =>{
  if (pass1.length>=8){
    if (pass1 === pass2){
      return true
    }
    else{
      return false
    }
  }
  else{
    return false
  }
}

class RegForm extends Component{
    constructor(props) {
        super(props);
        this.state = {
          firstName: '',
          secondName: '',
          email: '',
          password: '',
          passwordConfirm: '',
          error: 'Message',

          isLogining:false,

          emailCheck: true,
          passCheck: true,
          secondNameCheck:true,
          firstNameCheck:true,

        };
      }

      handleInput = (event)=>{
        this.setState({
          [event.target.name]: event.target.value
        })
      }

      handleSubmit = (event) => {
        event.preventDefault();
        
        this.setState({
          firstNameCheck : (this.state.firstName!='')?true:false,
          secondNameCheck : (this.state.secondName!='')?true:false,
          emailCheck : validEmailRegex.test(this.state.email) ? true : false,
          passCheck : check(this.state.password, this.state.passwordConfirm),
        },

        ()=>{
          if  (this.state.emailCheck === true && this.state.passCheck === true && this.state.secondNameCheck === true && this.state.firstNameCheck === true){
            console.log(this.state)
            Axios.post("http://localhost:8000/api/v1/users/register/", {
                  username: this.state.firstName+' '+this.state.secondName,
                  email: this.state.email,
                  password: this.state.password
                })
              .then(response => {
                this.setState({isLogining:false})
                console.log(response.data)
                if (response.data.success){
                  localStorage.setItem('userToken',response.data.token);
                  localStorage.setItem('role',response.data.position);
                  window.location.href = '/dashboard'
                }
              })
              .catch(error => {
                this.setState({isLogining:false})
                if (error.response) {
                  // The request was made and the server responded with a status code
                  // that falls out of the range of 2xx
                  alert(error.response.data.email[0]);
                } else if (error.request) {
                  console.log(error.request);
                } else {
                  // Something happened in setting up the request that triggered an Error
                  console.log('Error', error.message);
                }
              });
          } 
          else{
            this.setState({isLogining:false})
            console.log(this.state)
            console.log("Incorrect Form")
          }
        }
        )

        
        
      }


    render(){
        return(
            <Form>
              <Form.Group controlId="formBasicText">
                <Form.Control isInvalid={!this.state.firstNameCheck} type="text" name='firstName' placeholder="First Name" onInputCapture={this.handleInput}/>
                <Form.Control.Feedback type="invalid">
                  First Name should not be empty
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formBasicTextt">
                <Form.Control isInvalid={!this.state.secondNameCheck} type="text" name='secondName' placeholder="Second Name" onInputCapture={this.handleInput} />
                <Form.Control.Feedback type="invalid">
                  Second Name should not be empty
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Control type="email" isInvalid={!this.state.emailCheck} name='email' placeholder="Email address" onInputCapture={this.handleInput}/>
                <Form.Control.Feedback type="invalid">
                  Please, write correct email
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Control type="password" name='password' placeholder="Password" onInputCapture={this.handleInput} />
              </Form.Group>
              <Form.Group controlId="passwordConfirm">
                <Form.Control isInvalid={!this.state.passCheck} type="password" name = 'passwordConfirm' placeholder="Confirm Password" onInputCapture={this.handleInput} />
                <Form.Control.Feedback type="invalid">
                  Passwords do not match!
                </Form.Control.Feedback>
              </Form.Group>
              {
                this.state.isLogining?<div>Registering...</div>:
                <Button disabled={this.state.isLogining} variant="primary" type="submit" onClick={this.handleSubmit} >
                  Register
              </Button>
              }
              
            </Form>
        )

    }
}

export default RegForm
