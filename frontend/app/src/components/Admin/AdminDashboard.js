import React, { Component } from 'react'
import AdminDashboardContent from './Components/AdminDashboardContent'
import AdminDashboardSidebar from './Components/AdminDashboardSidebar'
import AdminDashboardNavbar from './Components/AdminDashboardNavbar'
import './css.css'
export default class AdminDashboard extends Component {
    
    state = {
        isAdmin:false
    }
    componentDidMount() {
        // const userToken = localStorage.getItem('userToken');
        // const role = localStorage.getItem('admin');
        // if (userToken&&role){
        //     this.setState({
        //         isAdmin:true
        //     })
        // }
        // else{
        //     window.location.href = '/'
        // }
        
	

	// Set initial active toggle
        this.setState({
            isAdmin:true
        })
    }
    render() {
        if (!this.state.isAdmin) { return null }
        return (
            <div >
                <AdminDashboardNavbar />
                <AdminDashboardSidebar />
                <AdminDashboardContent />
            </div>
        )
    }
}



