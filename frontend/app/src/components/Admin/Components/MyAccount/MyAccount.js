/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
const courses = [
    {
        id: 1,
        courseName: 'Mathematics',
        icon: 'math.svg',
    },
    {
        id: 2,
        courseName: 'English',
        icon: 'english.svg',
    },
    {
        id: 3,
        courseName: 'Science',
        icon: 'science.svg',
    },
    {
        id: 4,
        courseName: 'Life Skills',
        icon: 'lifeSkills.svg',
    },
    {
        id: 1,
        courseName: 'Article Writting',
        icon: 'writing.svg',
    },
    {
        id: 1,
        courseName: 'Exercises',
        icon: 'exercise.svg',
    },
]
class MyAccount extends React.Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-lg-7">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Personal information </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <form>
                                            <div className="form-group">
                                                <label className="label">First Name</label>
                                                <input type="text" className="form-control" id placeholder="First Name" defaultValue="Meerbek Akimzhanov" />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Email</label>
                                                <input type="email" className="form-control" id placeholder="Email" defaultValue="meerbek.kg@gmail.com" readOnly />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Mobile</label>
                                                <input type="text" className="form-control" id placeholder="Mobile" defaultValue={996194193} />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Country</label>
                                                <select  className="form-control text-dark" >
                                                    <option value="Male" key="Male">Kyrgysztan</option>
                                                    <option value="Female" key="Male">Kazakhstan</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Gender</label>
                                                <select  className="form-control text-dark" >
                                                    <option value="Male" key="Male">Male</option>
                                                    <option value="Female" key="Male">Female</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <img src="https://tutoria.pk/public/assets/assets-app/img/profile_img2.png" alt="..." className="img-thumbnail" />
                                        <button class="btn btn-primary float-right mt-1" type="submit" >Change</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Educational information </div>
                                <div className="row">
                                    <div className="col">
                                        <form>
                                            <div className="form-group">
                                                <label className="label">School Name</label>
                                                <input type="text" className="form-control" id placeholder="Your Board" defaultValue="UCA" readOnly />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Grade</label>
                                                <input type="text" className="form-control" id placeholder="Your Grade" defaultValue="Grade 11" readOnly />
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-5">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Change password</div>
                                <div className="row">
                                    <div className="col">
                                        <form method="POST" action="change-password">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="form-group required">
                                                        <input type="password" name="old_password" id="input-current-password" className="form-control " placeholder="Current Password"  required />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input type="password" name="password" id="input-password" className="form-control" placeholder="New Password"  required />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input placeholder="Confirm New Password" id="input-password-confirmation" name="password_confirmation" type="password" className="form-control"  required />
                                                    </div>
                                                </div>
                                                <div className="col-12">
                                                    <input className="btn btn-primary float-right" type="submit" defaultValue="update" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Your subscription details</div>
                                <div className="row">
                                    <div className="col">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr className="text-primary h6">
                                                    <th>Subscription</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Englsih</td>
                                                    <td>0.00</td>
                                                    <td>Active</td>
                                                    <td>2020-05-20 12:44:31</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Your payment details</div>
                                <div className="row">
                                    <div className="col">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr className="text-primary h6">
                                                    <th>Amount</th>
                                                    <th>Transaction number</th>
                                                    <th>GateWay</th>
                                                    <th>Method</th>
                                                    <th>Voucher #</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colSpan={7} className="text-center text-primary font-italic">No Transaction Found</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default MyAccount