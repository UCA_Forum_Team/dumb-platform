import React, { Component } from 'react'
import MyBag from './MyBag/MyBag'
import MyAccount from './MyAccount/MyAccount'
import Upgrade from './Upgrade/Upgrade'

import { Route, Switch } from 'react-router-dom';



export default class DashboardContent extends Component {
    render() {
        return (
            <main className="app-content">
                <div className="app-title">
                    <div>
                        <h1><i className="fa fa-dashboard" /> Dashboard</h1>
                    </div>
                    <ul className="app-breadcrumb breadcrumb">
                        <li className="breadcrumb-item"><i className="fa fa-home fa-lg" /></li>
                        <li className="breadcrumb-item"><a href="#">Dashboard</a></li>
                    </ul>
                </div>
                <div className="row">
                    <div className="col-md-6 col-lg-3">
                        <div className="widget-small primary coloured-icon"><i className="icon fa fa-users fa-3x" />
                            <div className="info">
                                <h4>Users</h4>
                                <p><b>5</b></p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="widget-small info coloured-icon"><i className="icon fa fa-thumbs-o-up fa-3x" />
                            <div className="info">
                                <h4>Courses</h4>
                                <p><b>25</b></p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="widget-small warning coloured-icon"><i className="icon fa fa-files-o fa-3x" />
                            <div className="info">
                                <h4>Articles</h4>
                                <p><b>10</b></p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3">
                        <div className="widget-small danger coloured-icon"><i className="icon fa fa-star fa-3x" />
                            <div className="info">
                                <h4>Mentors</h4>
                                <p><b>500</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="tile">
                            <h3 className="tile-title">Monthly Sales</h3>
                            <div className="embed-responsive embed-responsive-16by9">
                                <canvas className="embed-responsive-item" id="lineChartDemo" width={475} height={267} style={{ width: '475px', height: '267px' }} />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="tile">
                            <h3 className="tile-title">Support Requests</h3>
                            <div className="embed-responsive embed-responsive-16by9">
                                <canvas className="embed-responsive-item" id="pieChartDemo" width={475} height={267} style={{ width: '475px', height: '267px' }} />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="tile">
                            <h3 className="tile-title">Striped Table</h3>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="tile">
                            <h3 className="tile-title">Contextual Classes</h3>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr className="table-info">
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr className="table-success">
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr className="table-danger">
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                    <tr className="table-warning">
                                        <td>4</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}



