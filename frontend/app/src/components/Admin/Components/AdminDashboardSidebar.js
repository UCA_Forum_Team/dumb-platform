import React, { Component } from 'react'
import {
    NavLink
} from "react-router-dom";
import {Accordion} from "react-bootstrap";

export default class DashboardNavbar extends Component {
    render() {
        return (
            <aside className="app-sidebar">
                <div className="app-sidebar__user"><img className="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image" />
                    <div>
                        <p className="app-sidebar__user-name">John Doe</p>
                        <p className="app-sidebar__user-designation">Admin</p>
                    </div>
                </div>
                <ul className="app-menu">
                    <li><a className="app-menu__item active" href="dashboard.html"><i className="app-menu__icon fa fa-dashboard" /><span className="app-menu__label">Dashboard</span></a></li>
                    <li className="treeview is-expanded">
                        <Accordion>
                            <Accordion.Toggle variant="link" eventKey="0"><a className="app-menu__item" href="#" data-toggle="treeview"><i className="app-menu__icon fa fa-users" /><span className="app-menu__label">Users</span><i className="treeview-indicator fa fa-angle-right" /></a></Accordion.Toggle>
                            
                            <Accordion.Collapse eventKey="0">
                            <ul className="treeview-menu">
                                <li><NavLink to="/admin/students" activeClassName="active" className="treeview-item" href="#"><i className="icon fa fa-graduation-cap" /> Students</NavLink></li>
                                <li><NavLink to="/admin/teachers" activeClassName="active" className="treeview-item" href="#"><i className="icon fa fa-address-book" /> Teachers</NavLink></li>
                                <li><NavLink to="/admin/guests" activeClassName="active" className="treeview-item" href="#"><i className="icon fa fa-user-circle" /> Guests</NavLink></li>
                            </ul>
                            </Accordion.Collapse>
                        </Accordion>
                    </li>
                    
                    <li><a className="app-menu__item" href="#"><i className="app-menu__icon fa fa-book" /><span className="app-menu__label">Courses</span></a></li>
                    <li><a className="app-menu__item" href="#"><i className="app-menu__icon fa fa-pie-chart" /><span className="app-menu__label">Articles</span></a></li>
                </ul>
            </aside>
        )
    }
}



