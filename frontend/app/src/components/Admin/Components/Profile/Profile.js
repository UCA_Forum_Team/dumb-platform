/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

class Profile extends React.Component {
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-lg-7">
                        <div className="card shadow mb-4">
                            <div classNameName="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">personal information </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <form>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="" placeholder="First Name"
                                                    value="" readonly="" />
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="" placeholder="Mobile"
                                                    value="" readonly="" />
                                            </div>
                                            <div className="form-group">
                                                <input type="email" className="form-control" id="" placeholder="Email"
                                                    value="" readonly="" />
                                            </div>
                                        </form>

                                    </div>

                                    <div className="col-md-4 text-center">
                                        <img src="/src/components/Dashboard/Icons/user.svg" width="200px" alt="..."
                                            className="img-thumbnail" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">educational information </div>
                                <div className="row">
                                    <div className="col">
                                        <form>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="" placeholder="Your Board"
                                                    value="" readonly="" />
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="" placeholder="Your Grade"
                                                    value="" readonly="" />
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="" placeholder="Your Group"
                                                    value="" readonly="" />
                                            </div>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-5">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">change password</div>
                                <div className="row">
                                    <div className="col">
                                        <form method="POST" action="change-password">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="form-group required">
                                                        <input type="password" name="old_password"
                                                            id="input-current-password" className="form-control "
                                                            placeholder="Current Password" value="" required="" />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input type="password" name="password" id="input-password"
                                                            className="form-control" placeholder="New Password" value=""
                                                            required="" />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input placeholder="Confirm New Password"
                                                            id="input-password-confirmation"
                                                            name="password_confirmation" type="password"
                                                            className="form-control" value="" required="" />
                                                    </div>
                                                </div>
                                                <div className="col-12">
                                                    <input className="btn btn-primary float-right" type="submit"
                                                        value="update" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">your subscription details</div>
                                <div className="row">
                                    <div className="col">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr className="text-primary h6">
                                                    <th>subscription</th>
                                                    <th>amount</th>
                                                    <th>status</th>
                                                    <th>date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>

        )
    }
}

export default Profile