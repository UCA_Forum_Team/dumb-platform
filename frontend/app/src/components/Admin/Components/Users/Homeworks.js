/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
const homeworks = [
    {
        id: 1,
        courseName: 'Mathematics',
        desciption: 'Something for description',
        detail:'More detailedinformation given by teacjer',
        homeworkCreated: '02.03.2020',
        homeworkDueTo: '04.03.2020'
    },
    {
        id: 2,
        courseName: 'Logic',
        desciption: 'Something for description',
        detail:'More detailedinformation given by teacjer',
        homeworkCreated: '02.03.2020',
        homeworkDueTo: '04.03.2020'
    },
    {
        id: 3,
        courseName: 'English',
        desciption: 'Something for description',
        detail:'More detailedinformation given by teacjer',
        homeworkCreated: '02.03.2020',
        homeworkDueTo: '04.03.2020'
    },
    {
        id: 4,
        courseName: 'Russki',
        desciption: 'Something for description',
        detail:'More detailedinformation given by teacjer',
        homeworkCreated: '02.03.2020',
        homeworkDueTo: '04.03.2020'
    }
]
class MyBag extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="card shadow mb-4" id="subjects_list_container">
                        <div className="card-body">
                            <h5 className="m-0 font-weight-bold text-primary mb-3">Homeworks</h5>
                            <div className="row p-3">{
                                homeworks.map((homework)=>{
                                    return(<div className="col-sm-12 m-0 row homeworkRow mb-3 p-2">
                                    <div className="homeworkName col-10">
                                        
                                        {homework.courseName}
                                    </div>
                                    <div className="homeworkCourseName col-10">
                                        {homework.desciption}
                                    </div>
                                    <div className="homeworkDate col-2  ">
                                        <div className="col-sm-12">
                                            <span className="mr-1">Done !</span>
                                        <input type="checkbox" />
                                        </div>
                                    </div>
                                    <div className="homeworkDeadline col-sm-12">
                                            Deadline: {homework.homeworkDueTo}
                                    </div>
                                    
                                </div>)
                                })
                            }
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyBag