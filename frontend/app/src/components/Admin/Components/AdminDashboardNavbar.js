import React, { Component } from 'react'

export default class DashboardNavbar extends Component {
    logOut() {
        if (window.confirm("Do you really want to quit?")) {
            localStorage.removeItem('userToken')
            localStorage.removeItem('role')
            window.location.href = '/'
        }

    }
    render() {
        return (
            <header className="app-header"><a className="app-header__logo" href="/" style={{display: "flex",justifyContent: "center",alignItems: "center"}}><img src={require("../Icons/logo2.png")} alt="toDoList" height={30} /></a>
                {/* Navbar Right Menu*/}
                <ul className="app-nav">
                    
                    
                    {/* User Menu*/}
                    <li className="dropdown"><a className="app-nav__item" href="/#" data-toggle="dropdown" aria-label="Open Profile Menu"><i className="fa fa-user fa-lg" /></a>
                        <ul className="dropdown-menu settings-menu dropdown-menu-right">
                            <li><a className="dropdown-item" href="page-user.html"><i className="fa fa-user fa-lg" /> Profile</a></li>
                            <li><a className="dropdown-item" onClick={this.logOut} href="/#"><i className="fa fa-sign-out fa-lg" /> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </header>
        )
    }
}



