/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
const courses = [
    {
        id:1,
        courseName: 'Mathematics',
        icon: 'math.svg',
    },
    {
        id:2,
        courseName: 'English',
        icon: 'english.svg',
    },
    {
        id:3,
        courseName: 'Science',
        icon: 'science.svg',
    },
    {
        id:4,
        courseName: 'Life Skills',
        icon: 'lifeSkills.svg',
    },
    {
        id:1,
        courseName: 'Article Writting',
        icon: 'writing.svg',
    },
    {
        id:1,
        courseName: 'Exercises',
        icon: 'exercise.svg',
    },
]
class MyBag extends React.Component{
    render(){
        return(
            <div className="row">
            <div className="col-lg-12">
                <div className="card shadow mb-4" id="subjects_list_container">
                    <div className="card-body">
                        <h5 className="m-0 font-weight-bold text-primary mb-3">My courses</h5>
                        <div className="row">
                            {
                                courses.map((course,index)=>{
                                    return (
                                        <div className="col-md-4 mb-2">
                                            <div className="card shadow h-100 py-auto">
                                                <div className="card-body py-2 px-3">
                                                    <a className="d-flex align-items-center"
                                                        href="#">
                                                        <img className="img-fluid subject-icon"
                                                            src={require("../../Icons/"+course.icon)}/>
                                                        <span className="h6 font-weight-bold text-dark-purple mb-0">
                                                            {course.courseName} 
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default MyBag