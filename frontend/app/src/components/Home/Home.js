import React, { Component } from 'react';
import './home.css';

import logo1 from './elcart.png';
import logo2 from "./MP.png";

// import OwlCarousel from 'react-owl-carousel';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel/dist/assets/owl.theme.default.css';




export default class Home extends Component {
  render() {

    return (
      <React.Fragment>
        {/* The Side sticky icons */}
        <div class="sticky_icons d-none d-sm-block">
          <ul class="list-unstyled">
            <li>Pay via</li>
            <li class="pb-4">
              <img width="50" alt="elcart logo" src={logo1} />
            </li>
            <li class="pb-4">
              <img width="50" alt="MegaPay logo" src={logo2} />

            </li>
            <li>
              <img width="50" alt="logo" src="https://static-2.akipress.org/st_runews/2/1434222.0.1519799427_0.jpg" />
            </li>
          </ul>
        </div>
        {/* This is the end of Stikcy Icons */}
        {/* -------------The Main Banner ----------------------*/}
        <section class="banner-section mb-sm-5">

          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-8 my-auto">
                <h1 class="title">Drive Ur <br />Mind Bravely</h1>

                <h3 class="text-primary font-nm mb-2 mb-sm-3">
                  Solve Problems &amp; build your Mind,<br></br> Work out &amp; Build Your Body
                          </h3>
                <div>
                  <a class="btn btn-banner d-none d-sm-inline-block" href="/aboutus">Learn more</a>
                  <a class="btn btn-banner d-inline-block d-sm-none" href="#">free trial</a>
                </div>
              </div>
              <div class="col-lg-6 col-md-4 col-3 my-auto d-none d-sm-block">
                <img class="img-fluid imgBanner" alt="Banner Image" src="https://www.arenasimulation.com/public/uploads/images/general/studentpic1.png" />
              </div>
            </div>
          </div>
        </section>
        <section className="section_gap">
          <div className="container">
            <div className="row mb-5">
              <div className="col-md-12 text-sm-center banner-section">
                <h1 class="title  text-sm-center">3 Categories</h1>

                <p className="h5 mt-4">
                  All our categories are customized to fulfill your specific needs so you get the best result
              </p>
              </div>
            </div>
            <div className="d-flex flex-column flex-md-row justify-content-md-center row">
              <div className="card mb-4 mr-md-4 shadow border-0 text-center col-3">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title h4 text-primary-light mb-4">FAQ</h1>
                  <p className="text-center"><img style={{ width: '100%', height: 'auto' }} src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/e35/101809423_165644404988589_762186279767749433_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=109&_nc_ohc=LFLLvIeTpOcAX-mKEDY&oh=5b1dc32c0d5194238bbbb52606a16935&oe=5F57FE67" height={110} /></p>
                  <h5>bla bla bla</h5>
                  <a href="https://tutoria.pk/app/subscriptionplans" className="btn btn-primary mt-2">Get started
                  Now</a>
                </div>
              </div>
              <div className="card mb-4 mr-md-4 shadow border-0 text-center col-3">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title h4 text-primary-light mb-4"> Mind</h1>
                  <p className="text-center">
                    <img style={{ width: '100%', height: 'auto' }} src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/101384418_278068076716111_6012556175909717647_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=107&_nc_ohc=n1I5FkzwEIEAX9yTx9L&oh=d7e9c972ba38b0c33f46eb3470030e28&oe=5F5A0355" height={110} />
                  </p>
                  <h5>bla bla bla</h5>
                  <a href="https://tutoria.pk/app/subscriptionplans" className="btn btn-primary mt-2">Get started
                  Now</a>
                </div>
              </div>
              <div className="card mb-4 shadow border-0 text-center col-3">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title h4 text-primary-light mb-4"> Body</h1>
                  <p className="text-center">
                    <img style={{ width: '100%', height: 'auto' }} src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/e35/81820200_1668293746663611_7364791718387688353_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=103&_nc_ohc=3U5HDxob1mYAX-mLrSY&oh=5c3264eae62e1be5d7862a6065352270&oe=5F599BDC" height={110} />
                  </p>
                  <h5>bla bla bla</h5>
                  <a href="https://tutoria.pk/app/subscriptionplans" className="btn btn-primary mt-2">Get started
                  Now</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section_gap">
          <div className="container">
            <div className="row ">
              <div className="col-md-12 banner-section">
                <h1 className="title text-center"><span className="text-purple">Why choose </span>
                  <span className="">opendelta.org?</span>
                </h1>
                <p className="h5 mt-4">
                  Opendelta is not just an e-learning platform, it is the gateway towards securing a
                  brighter future for your academic and professional life.
              </p>
              </div>
            </div>
            <div className="row mt-4 feature-section">
              <div className="col-md-6">
                <div className="d-flex justify-content-between mb-3">
                  <div className="mr-sm-5 mr-3 col-2">
                    <img src="https://tutoria.pk/public/assets/assets-site/images/icon1.svg" className="img-fluid" />
                  </div>
                  <div>
                    <h4 className="card-title">Learn with any device</h4>
                    <p className="card-text">
                      Unlike traditional studying, you can now prepare for your exams even while
                      on
                      the go. through tutoria’s smart learning application, you get the best
                      cross-platform experience on your smartphone, tablet, or laptop, right on
                      your
                      fingertips
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="d-flex justify-content-between mb-3">
                  <div className="mr-sm-5 mr-3 col-2">
                    <img src="https://tutoria.pk/public/assets/assets-site/images/fast_support.svg" className="img-fluid" />
                  </div>
                  <div>
                    <h4 className="card-title">Fastest support</h4>
                    <p className="card-text">
                      Friendly and highly knowledgeable, our support team is always on the lookout
                      to
                      help you with anything that crosses your mind. our dedicated support team is
                      available 24 hours a day and 7 days a week so you get your answers without
                      losing a minute
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="d-flex justify-content-between mb-3">
                  <div className="mr-sm-5 mr-3 col-2">
                    <img src="https://tutoria.pk/public/assets/assets-site/images/own_time.svg" className="img-fluid" />
                  </div>
                  <div>
                    <h4 className="card-title">Study at your own time</h4>
                    <p className="card-text">
                      Every student has a unique learning curve based on their natural speed and
                      need.
                      tutoria gives you the ultimate independence of self-pacing your exam
                      preparation, anytime around the clock, so studying for exams becomes a walk
                      in
                      the park
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="d-flex justify-content-between mb-3">
                  <div className="mr-sm-5 mr-3 col-2">
                    <img src="https://tutoria.pk/public/assets/assets-site/images/certified_teachers.svg" className="img-fluid" />
                  </div>
                  <div>
                    <h4 className="card-title">Proffesional Mentors</h4>
                    <p className="card-text">
                      Our study materials are the workmanship of Ph.D level subject specialists
                      who
                      thoroughly process and vet every aspect of the content. this rigorous
                      testing
                      helps us ensure the highest standard of quality and deliver the best
                      academic
                      value to you
                  </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container mt-5 mb-5 banner-section">
            <h1 className="text-center">Our mentors</h1>
            {/* <OwlCarousel
              autoplay={true}
              smartSpeed={5000}
              items={4}
              className="owl-theme"
              loop
              margin={10}
            >
              <div class="item"><div className="team-thumb">
                <div className="team-image">
                  <img alt="" className="img-responsive" src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/106547770_271564957458953_444880774374859087_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=105&_nc_ohc=prkBjE5fGWgAX_yY3ID&oh=19f9164f678e2142f082ab2ef5586fb4&oe=5F5A75A3" />
                </div>
                <div className="team-info">
                  <h4>Наита </h4><span />
                  <p><span>Президент </span></p><span />
                </div>
                <ul className="social-icon">
                  <li>
                    <a className="fa fa-envelope-square" href="mailto:azharazizbekova@gmail.com" />                              </li>
                  <li>
                  </li>
                  <li>
                    <a className="fa fa-instagram" style={{ marginLeft: '10%' }} href />                              </li>
                  <li>
                    <a className="fa fa-linkedin" style={{ marginLeft: '10%' }} href />                              </li>
                </ul>
              </div></div>
              <div class="item"><div className="team-thumb">
                <div className="team-image">
                  <img alt="" className="img-responsive" src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/106486211_313470933007120_5810375095963298537_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=5aSyAzID2i8AX-lo23h&oh=0edb362e54b287fd5c3ed84f8a9bdfb1&oe=5F5A5F02" />
                </div>
                <div className="team-info">
                  <h4>Асылбек </h4><span />
                  <p><span>Президент </span></p><span />
                </div>
                <ul className="social-icon">
                  <li>
                    <a className="fa fa-envelope-square" href="mailto:azharazizbekova@gmail.com" />                              </li>
                  <li>
                  </li>
                  <li>
                    <a className="fa fa-instagram" style={{ marginLeft: '10%' }} href />                              </li>
                  <li>
                    <a className="fa fa-linkedin" style={{ marginLeft: '10%' }} href />                              </li>
                </ul>
              </div></div>
              <div class="item"><div className="team-thumb">
                <div className="team-image">
                  <img alt="" className="img-responsive" src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/106565338_200018274720674_3426499364634904736_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=102&_nc_ohc=D7OJXaTd2foAX_OAztl&oh=644157ac61289b63f5ec46009e69c02b&oe=5F5AF416" />
                </div>
                <div className="team-info">
                  <h4>Фирюза     </h4><span />
                  <p><span>Президент </span></p><span />
                </div>
                <ul className="social-icon">
                  <li>
                    <a className="fa fa-envelope-square" href="mailto:azharazizbekova@gmail.com" />                              </li>
                  <li>
                  </li>
                  <li>
                    <a className="fa fa-instagram" style={{ marginLeft: '10%' }} href />                              </li>
                  <li>
                    <a className="fa fa-linkedin" style={{ marginLeft: '10%' }} href />                              </li>
                </ul>
              </div></div>
              <div class="item"><div className="team-thumb">
                <div className="team-image">
                  <img alt="" className="img-responsive" src="https://instagram.ffru2-1.fna.fbcdn.net/v/t51.2885-15/e35/106557502_172711577559750_4284217895957185061_n.jpg?_nc_ht=instagram.ffru2-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=QFhaRhufHA4AX8y-fsR&oh=63bffd9d125fc32d8faedfcd63a8f710&oe=5F59CE96" />
                </div>
                <div className="team-info">
                  <h4>Аэлита </h4><span />
                  <p><span>Президент </span></p><span />
                </div>
                <ul className="social-icon">
                  <li>
                    <a className="fa fa-envelope-square" href="mailto:azharazizbekova@gmail.com" />                              </li>
                  <li>
                  </li>
                  <li>
                    <a className="fa fa-instagram" style={{ marginLeft: '10%' }} href />                              </li>
                  <li>
                    <a className="fa fa-linkedin" style={{ marginLeft: '10%' }} href />                              </li>
                </ul>
              </div></div>


            </OwlCarousel> */}
          </div>
        </section>
        <section className="section_gap">
          <div className="container">
            <div className="row">
              <div className="col-md-6 my-sm-auto mb-3">
                <div className="position-relative">
                  <img className="img-fluid" src="https://tutoria.pk/public/assets/assets-site/images/feedback.webp" alt="" />
                </div>
              </div>
              <div className="col-md-6 my-auto order-sm-first">
                <h2 className="title2">
                  <span className="text-purple">Students’</span><br />
                  <span className="text-white">feedback</span>
                </h2>
                <p className="h5 mt-4">
                  “I’m very dedicated to getting good marks in my exams. But since starting
                  intermediate,
                  I’ve struggled to keep up with everything that I have to learn in class. Finding
                  tutoria.pk saved me and my grades. Now, I can prepare for my exams with full
                  confidence
                  because I know that these solutions are high-quality.“
              </p>
                <div className="mt-4">
                  <h4 className="mb-1 text-purple">Aliya Bektemirova</h4>
                  <span className="text-purple">Our student from Kyrgyzstan</span>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/*------------ End of Banner Section------- */}
        {/*-------------------- The Three fliping Cards---------------- */}
        {/* -----------The End of Fliping Cards------------------------ */}


      </React.Fragment>

      // <Container>
      //     <Row>
      //         <Col>
      //         <h1>Hello word</h1>
      //         </Col>
      //     </Row>
      // </Container>
    );
  }
}