import React, { Component } from "react";
import "./style.css";
import {
  Navbar,
  Nav,
  Button,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import Image from "react-bootstrap/Image";
import Logo from "./logo2.png";

class Navigation extends Component {
  state = {
    isLogined:false
  }
  componentDidMount() {
    const userToken = localStorage.getItem('userToken');
    if (userToken){
      this.setState({
        isLogined:true
      })
    }
    
  }



  render() {
    return (
      <>
      <div className="hide" id="hide">  
        <p><FontAwesomeIcon icon={faPhone} />  +996-777 438 662</p>
      </div>
      <div className="hooman" id="nav">
        
        <div className="container">
          <Navbar expand="lg" className="hello">
            <Navbar.Brand href="/"><Image style={{marginTop:'-13%', marginBottom:'-10%'}} src={Logo} height="40" alt="Picture"/></Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse>
              <Nav className="ml-auto">
                <Nav.Item className="nada">
                  <NavLink className="nav-link" activeClassName="active" to="Aboutus">
                    <p>About us</p>
                  </NavLink>
                </Nav.Item>
                <Nav.Item className="nada">
                  <NavLink className="nav-link" activeClassName="active" to="Courses">
                    <p>Courses</p>
                  </NavLink>
                </Nav.Item>
                <Nav.Item className="nada">
                  <NavLink className="nav-link" activeClassName="active" to="Mentoring">
                    <p>Mentoring</p>
                  </NavLink>
                </Nav.Item>
                <Nav.Item className="nada">
                  <NavLink className="nav-link" activeClassName="active" to="Articles">
                    <p>Articles</p>
                  </NavLink>
                </Nav.Item>
                {
                  this.state.isLogined?
                  <Nav.Item className="nada er">
                    <a className="nav-link" activeClassName="active" href="Dashboard">
                      <Button variant="outline-info">Dashboard</Button>
                    </a>
                  </Nav.Item>
                  :
                  <>
                  <Nav.Item className="nada">
                    <NavLink className="nav-link" activeClassName="active" to="Login">
                      <p>Login</p>
                    </NavLink>
                  </Nav.Item>
                  <Nav.Item className="nada er">
                    <NavLink className="nav-link" activeClassName="active" to="Register">
                      <Button variant="outline-info">Register</Button>
                    </NavLink>
                  </Nav.Item>
                  </>
                }
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </div>
      </>
    );
  }
}

export default Navigation;
