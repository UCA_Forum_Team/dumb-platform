import React, { Component } from 'react';
import Axios from "axios";


const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach(
    (val) => val.length > 0 && (valid = false)
  );
  return valid;
}

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      response: '',
      errors: {
        email: ''
      }
    };
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'email': 
        errors.email = 
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});
  }

  // handleSubmit = (event) => {
  //   event.preventDefault();
  //   console.log(validateForm(this.state.errors))
  //   if(validateForm(this.state.errors)) {
  //     Axios.post("http://localhost:8000/api/v1/users/login/", {
  //               email: this.state.email,
  //               password: this.state.password
  //             })
  //           .then(response => console.log(response.data))
  //           .catch(error => console.log(error));
  //   }else{
  //     console.error('Invalid Form')
  //   }
  // }

  render() {
    const {errors} = this.state;
    return (
      <div className='wrapper'>
        <div className='form-wrapper'>
          <form onSubmit={this.handleSubmit} noValidate>
            <div className='email'>
              <input 
                type='email' 
                name='email' 
                onChange={this.handleChange} 
                placeholder='Email' 
                noValidate /><br/>

              {errors.email.length > 0 && 
                <span className='error'>{errors.email}</span>}
            </div>
            <div className='submit' onClick={this.handleSubmit}>
              <button>Send Message</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}


export default Register



