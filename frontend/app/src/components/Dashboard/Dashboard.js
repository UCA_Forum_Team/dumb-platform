import React, { Component } from 'react'
import DashboardContent from './Components/DashboardContent'
import DashboardSidebar from './Components/DashboardSidebar'
// import "./all.css";
import "./css.css";
import "./styles.css";

export default class Dashboard extends Component {
    state = {
        isLogined:false
    }
    componentDidMount() {
        const userToken = localStorage.getItem('userToken');
        if (userToken){
            this.setState({
                isLogined:true
            })
        }
        else{
            window.location.href = '/'
        }
    }
    render() {
        if (!this.state.isLogined) { return null }
        return (
            <div className="wrapper d-flex" >
                <DashboardSidebar />
                <DashboardContent />
            </div>
        )
    }
}



