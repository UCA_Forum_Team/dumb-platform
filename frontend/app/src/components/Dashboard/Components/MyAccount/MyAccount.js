/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
import Axios from 'axios'
class MyAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "data": {
                "id": 0,
                "email_id": "",
                "username": "",
                "birth_date": "",
                "gender": "",
                "photo": "",
                "country": "",
                "phone_number": "",
                "email": "",
                "has_card": false
            },
            isLoading: true,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        Axios.post("http://localhost:8000/api/v1/users/get_student/", null, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('userToken')
            }
        })
            .then(response => {
                if (response.data.success) {
                    this.setState({
                        isLoading: false,
                        data: response.data.result
                    })
                } else {
                    console.log(response.data)
                    this.setState({
                        isLoading: false,
                    })
                }

            })
            .catch(error => {
                console.log(error.data)
                this.setState({
                    isLoading: false,
                })

            });
    }
    handleInputChange(event) {

        this.setState({
            data: {                   // object that we want to update
                ...this.state.data,    // keep all other key-value pairs
                [event.target.name]: event.target.value       // update the value of specific key
            }
        })
    }
    changeUserHandler() {
        this.setState({
            isLoading: true
        })
        Axios.post("http://localhost:8000/api/v1/users/update_student/", 
        this.state.data, 
        {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('userToken')
            }

        }).then(response => {
            if (response.data.success) {
                this.setState({
                    isLoading: false
                })
            } else {
                console.log(response.data)
                this.setState({
                    isLoading: false,
                })
            }

        })
            .catch(error => {
                console.log(error.data)
                this.setState({
                    isLoading: false,
                })

            });

    }
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-lg-7">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Personal information </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <form>
                                            <div className="form-group">
                                                <label className="label">Birth date</label>
                                                <input type="text" className="form-control" name="birth_date" onChange={this.handleInputChange} placeholder="First Name" defaultValue={this.state.data.birth_date} />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Mobile</label>
                                                <input type="text" className="form-control" placeholder="Mobile" name="phone_number" onChange={this.handleInputChange} defaultValue={this.state.data.phone_number} />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Country</label>
                                                <select className="form-control text-dark" name="country" onChange={this.handleInputChange} defaultValue={this.state.data.country}>
                                                    <option value="Kyrgysztan" key="Kyrgysztan">Kyrgysztan</option>
                                                    <option value="Kazakhstan" key="Kazakhstan">Kazakhstan</option>
                                                    <option value="Абхазия" key="Абхазия" >Абхазия</option>
                                                    <option value="Белорусия" key="Белорусия" >Белорусия</option>
                                                    <option value="Болгария" key="Болгария" >Болгария</option>
                                                    <option value="Грузия" key="Грузия" >Грузия</option>
                                                    <option value="Казахстан" key="Казахстан" >Казахстан</option>
                                                    <option value="Киргизия" key="Киргизия" >Киргизия</option>
                                                    <option value="Латвия " key="Латвия " >Латвия </option>
                                                    <option value="Литва" key="Литва" >Литва</option>
                                                    <option value="Монголия" key="Монголия" >Монголия</option>
                                                    <option value="Польша" key="Польша" >Польша</option>
                                                    <option value="Россия" key="Россия" >Россия</option>
                                                    <option value="Сербия" key="Сербия" >Сербия</option>
                                                    <option value="Таджикистан" key="Таджикистан" >Таджикистан</option>
                                                    <option value="Узбекистан" key="Узбекистан" >Узбекистан</option>
                                                    <option value="Украина" key="Украина" >Украина</option>
                                                    <option value="Черногория" key="Черногория" >Черногория</option>
                                                    <option value="Чехия" key="Чехия" >Чехия</option>
                                                    <option value="Эстония" key="Эстония" >Эстония</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Gender</label>
                                                <select className="form-control text-dark" name="gender" onChange={this.handleInputChange} defaultValue={this.state.data.gender}>
                                                    <option value="Male" key="Male">Male</option>
                                                    <option value="Female" key="Female">Female</option>
                                                    <option value="Female" key="Female">Other</option>
                                                </select>
                                            </div>
                                            <button className="btn btn-primary float-right">Saves changes</button>
                                        </form>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <img src="https://tutoria.pk/public/assets/assets-app/img/profile_img2.png" alt="..." className="img-thumbnail" />
                                        <button className="btn btn-primary float-right mt-1" type="submit" >Change</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Account </div>
                                <div className="row">
                                    <div className="col">
                                        <form>
                                            <div className="form-group">
                                                <label className="label">Username</label>
                                                <input type="text" className="form-control" placeholder="Your Board" name="username" onChange={this.handleInputChange} defaultValue={this.state.data.username} readOnly />
                                            </div>
                                            <div className="form-group">
                                                <label className="label">Email</label>
                                                <input type="text" className="form-control" placeholder="Your Grade" name="email" onChange={this.handleInputChange} defaultValue="Email" readOnly />
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div className="h5 m-0 font-weight-bold text-primary mb-3 mt-4">Change password</div>
                                <div className="row">
                                    <div className="col">
                                        <form method="POST" action="change-password">
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="form-group required">
                                                        <input type="password" name="old_password" id="input-current-password" className="form-control " placeholder="Current Password" required />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input type="password" name="password" id="input-password" className="form-control" placeholder="New Password" required />
                                                    </div>
                                                </div>
                                                <div className="col-6">
                                                    <div className="form-group required">
                                                        <input placeholder="Confirm New Password" id="input-password-confirmation" name="password_confirmation" type="password" className="form-control" required />
                                                    </div>
                                                </div>
                                                <div className="col-12">
                                                    <input className="btn btn-primary float-right" type="submit" defaultValue="update" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <div className="h5 m-0 font-weight-bold text-primary mb-3">Your subscription details</div>
                                <div className="row">
                                    <div className="col">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr className="text-primary h6">
                                                    <th>Subscription</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Englsih</td>
                                                    <td>0.00</td>
                                                    <td>Active</td>
                                                    <td>2020-05-20 12:44:31</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}

export default MyAccount