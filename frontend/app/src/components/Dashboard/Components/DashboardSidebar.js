import React, { Component } from 'react'
import {
    NavLink
  } from "react-router-dom";
export default class DashboardNavbar extends Component {
    render() {
        return (
                <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                    {/* Sidebar - Brand */}
                    <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/Dashboard">
                        <div className="sidebar-brand-icon">
                            <img src={require("../Icons/logo2.png")} alt="toDoList" width={35} />
                        </div>
                    </a>
                    {/* Divider */}
                    <hr className="sidebar-divider my-0" />
                    {/* Nav Item - Dashboard */}
                    
                    <NavLink exact to="/Dashboard" activeClassName="active" className="nav-item">
                        <div className="nav-link">
                            <img src={require("../Icons/travel.svg")} alt="travel" />
                            <span>My bag</span>
                        </div>
                    </NavLink>
                    <NavLink to="/Dashboard/homeworks" activeClassName="active" className="nav-item">
                        <div className="nav-link" >
                            <img src={require("../Icons/interface.svg")} alt="interface" />
                            <span>Homeworks</span>
                        </div>
                    </NavLink>
                    
                    <NavLink to="/Dashboard/my-account" activeClassName="active" className="nav-item">
                        <div className="nav-link">
                            <img src={require("../Icons/user.svg")} alt="user" />
                            <span>My account</span>
                        </div>
                    </NavLink>
                    
                    
                    {/* Divider */}
                    
                </ul>
        )
    }
}



