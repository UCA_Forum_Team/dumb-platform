import React, { Component } from 'react'

export default class DashboardNavbar extends Component {
    logOut (){
        if (window.confirm("Do you really want to quit?")){
            localStorage.removeItem('userToken')
            localStorage.removeItem('role')
            window.location.href = '/'
        }
        
    }
    render() {
        return (
            <nav className="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                {/* Sidebar Toggle (Topbar) */}
                <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
                    <i className="fa fa-bars" />
                </button>
                {/* Topbar Navbar */}
                <ul className="navbar-nav ml-auto">
                    <div className="topbar-divider d-none d-sm-block" />
                    {/* Nav Item - User Information */}
                    <li className="nav-item dropdown no-arrow">
                        <a className="nav-link dropdown-toggle" href="/#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span className="mr-2 d-none d-lg-inline text-gray-600 small">Hello, Meerbek</span>
                            <img className="img-profile rounded-circle" src="https://tutoria.pk/public/assets/assets-app/img/profile_img.svg" alt="profile_img" width={60} />
                        </a>
                        {/* Dropdown - User Information */}
                        <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a className="dropdown-item" href="https://tutoria.pk/app/myaccount">
                                <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400" />
                                        Profile
                                    </a>
                            <div className="dropdown-divider" />
                            <a className="dropdown-item" href="/#" data-toggle="modal" onClick={()=>this.logOut()}>
                                <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400" />
                                    Logout
                                </a>
                        </div>
                    </li>
                </ul>
            </nav>
        )
    }
}



