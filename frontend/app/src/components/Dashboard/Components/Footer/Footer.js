import React from 'react';
import "./footer.css";
import "./dumb-project/frontend/app/src/components/Dashboard/all.css"
import "./dumb-project/frontend/app/src/components/Dashboard/styles.css"
import "./dumb-project/frontend/app/src/components/Dashboard/css.css"



class Footer extends React.Component {
  render() {
    return (
      <footer className="sticky-footer bg-white">
        <div className="container my-auto">
            <div className="copyright text-center my-auto">
                <span>powered and designed by D.U.M.M project | all rights reserverd © 2020</span>
            </div>
        </div>
      </footer>
    )
  }
}

export default Footer