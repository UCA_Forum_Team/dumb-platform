import React, { Component } from 'react'
import MyBag from './MyBag/MyBag'
import MyAccount from './MyAccount/MyAccount'
import Upgrade from './Upgrade/Upgrade'
import Homeworks from './Homeworks/Homeworks'

import {Route, Switch } from 'react-router-dom';

import DashboardNavbar from './DashboardNavbar'
// import "./all.css";
import "../css.css";
import "../styles.css";

export default class DashboardContent extends Component {
    render() {
        return (
            <div id="content-wrapper" className="d-flex flex-column" style={{width:'100%'}}>
                {/* Main Content */}
                <div id="content" className="page_bg">
                    {/* Including partial layout of navigation -- Topbar */}
                    {/* Topbar */}
                    <DashboardNavbar />
                    {/* End of Topbar */}
                    {/* Begin Page Content */}
                    <div className="scroll-view">
                        {/* Including partial layout of wallet */}
                        <div className="container-fluid">
                            {/*-My Wallet*/}
                            <div className="row mb-3">
                                <div className="col-auto font-weight-bold text-primary h4">
                                </div>
                                <div className="col-auto text-gray-900 ml-auto">
                                    <a className="text-blue" data-toggle="modal" data-target="#walletModal" data-keyboard="false" data-backdrop="static" type="button">
                                        <h6><span className="text-primary mr-2"><i className="fa fa-wallet" /> my wallet</span>
                                            <span className="text-danger" id="wallet_amount" name="wallet_amount">0.00</span>
                                        </h6>
                                    </a>
                                </div>
                            </div>
                            
                            
                            <Switch>
                                <Route path="/Dashboard/my-bag" component={MyBag} />
                                <Route path="/Dashboard/homeworks" component={Homeworks} />
                                <Route path="/Dashboard/my-account" component={MyAccount} />
                                <Route path="/Dashboard/upgrade" component={Upgrade} />
                                <Route component={MyBag} />
                            </Switch>
                        </div>
                        {/* Including partial layout of Footer */}
                        {/* Footer */}
                        <footer className="sticky-footer bg-white">
                            <div className="container my-auto">
                                <div className="copyright text-center my-auto">
                                    <span>Copyright © DUMB 2020</span>
                                </div>
                            </div>
                        </footer>
                        {/* End of Footer */}
                    </div>
                    {/* End of container-fluid */}
                </div>
                {/* End of Main Content */}
            </div>
        )
    }
}



