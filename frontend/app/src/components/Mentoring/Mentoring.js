import React, { Component } from "react";
import "./mentoring.css";
import { Container, Row} from "reactstrap";

export default class Courses extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.state = {
      show: false,
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <React.Fragment>
        <Container className="course">
          <Row style={{ textAlign: "center" }}>
            Mentoring Page
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}
