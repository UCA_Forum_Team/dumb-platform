import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import "./footer.css";
import {
  FaInstagram,
  FaFacebook,
  FaPhone,
  FaMapMarkerAlt,
  FaPhoneAlt,
  FaEnvelope,
} from "react-icons/fa";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container>
          <Row>
            <Col xs={12} md={7} style={{marginTop:'3%'}}>
              <h1>Need help choosing the right package?</h1>
              <h2 style={{ marginBottom: "5%" }}>Contact us now!</h2>
              <p style={{ fontSize: "18px" }}>
                <FaMapMarkerAlt
                  style={{ marginRight: "1%", marginTop: "-1%" }}
                />
                310 Lenin Street Naryn, 722918 Kyrgyzstan
              </p>
              <p style={{ marginTop: "-2%" }}>
                <FaPhoneAlt /> +996-777 438 662
              </p>
              <p style={{ marginTop: "-2%" }}>
                <FaEnvelope /> dumb-2021@gmail.com
              </p>
            </Col>
            <Col xs={6} md={3} style={{marginTop:'4%'}}>
              <h2 style={{ marginBottom: "8%" }}>Site map</h2>
              <ul>
                <li>
                  <a href="Aboutus">About us</a>
                </li>
                <li>
                  <a href="Courses">Courses</a>
                </li>
                <li>
                  <a href="Mentoring"> Mentoring</a>
                </li>
                <li>
                  <a href="Articles">Articles</a>
                </li>
                <li>
                  <a href="#">Terms and Services</a>
                </li>
                <li>
                  <a href="#">Privacy Policy</a>
                </li>
              </ul>
            </Col>
            <Col xs={6} md={2} style={{marginTop:'4%'}}>

              <ul className = "footerUl">
                <li style={{ marginBottom: "15%" }}>
                  <a href="https://www.instagram.com/open__delta/">
                    <FaInstagram size="30px" />
                  </a>
                </li>
                
                <li style={{ marginBottom: "15%" }}>
                  
                  <a href="tel:+996-777 438 662">
                  <FaPhoneAlt size="30px"/> 
                  </a>
                  
                </li>
                <li style={{ marginBottom: "15%" }}>
                  
                  <a href="mailto:dumb-2021@gmail.com">
                    <FaEnvelope size="30px" />
                  </a>
                  
                </li>
              </ul>
            </Col>
          </Row>
          <hr />
          <Row className="last-one">
            <Col xs={12} md={12} style={{textAlign:'center', marginTop:'1%', marginBottom:'1%'}}>
              <p style={{fontSize:'15px', color:'#fff'}}>Power and designed by d.u.m.b project | all rights reserverd &copy; 2020</p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
