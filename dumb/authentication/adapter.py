from rest_framework import exceptions
from allauth.account.adapter import DefaultAccountAdapter

class CustomAccountAdapter(DefaultAccountAdapter):

    def save_user(self, request, user, form, commit=False):
        user = super().save_user(request, user, form, commit)
        data = form.cleaned_data

        if data.get('email'):
            user.email = data.get('email')
            user.save()
            return user
        else:
            msg = f'There is no User with such Email!'
            raise exceptions.AuthenticationFailed(msg)
