from .models import Course, CourseHomeWork
from django.contrib import admin


admin.site.register(Course)
admin.site.register(CourseHomeWork)