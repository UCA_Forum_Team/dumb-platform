from . import serializers
from django.db import transaction
from users.models import Student, Teacher
from .models import Course, CourseHomeWork
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.authtoken.views import APIView
from authentication.backends import JWTAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser


class CourseListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        queryset = Course.objects.all().values()
        return Response(
            data=queryset,
            status=status.HTTP_200_OK
        )


class TeacherCoursesListView(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        try:
            teacher = Teacher.objects.get(teacher=request.user)
            courses_list = Course.objects.filter(owners=teacher)
            courses = {
                'success': True,
                'result': [course.name for course in courses_list]
            }
        except:
            courses = {
                'success': False,
                'message': 'Teacher does not exist',
                'result': [],
            }

        return Response(
            data=courses,
            status=status.HTTP_200_OK
        )


class StudentCoursesListView(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        try:
            student = Student.objects.get(student=request.user)
            course_list = Course.objects.filter(students=student)
            courses = {
                'success': True,
                'result': [course.name for course in course_list]
            }
        except:
            courses = {
                'success': False,
                'message': 'Student does not exist',
                'result': [],
            }

        return Response(
            data=courses,
            status=status.HTTP_200_OK
        )


class CourseStudentsList(generics.ListAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = serializers.CourseListSerializer
    queryset = Course.objects.all()


class CourseOwnersList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            course = Course.objects.filter(name=request.data["subject"].title()).first()
            teachers = course.owners.all().values(
                'username',
                'department',
                'campus',
                'country'
            )
        except AttributeError:
            teachers = {
                "success": True,
                "message": "Subject is not found",
                "result": []
            }

        except KeyError:
            teachers = {
                "success": False,
                "message": "You sent Empty SUBJECT",
                "result": []
            }

        return Response(
            data=teachers,
            status=status.HTTP_200_OK
        )


class CourseStudentsList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            course = Course.objects.filter(name=request.data["subject"].title()).first()
            children = course.students.all().values(
                'username',
                'gender',
                'country',
                'phone_number'
            )
        except AttributeError:
            children = {
                "success": True,
                "message": "Subject is not found" ,
                "result": []
            }
        except KeyError:
            children = {
                "success": False,
                "message": "You sent Empty SUBJECT",
                "result": []
            }

        return Response(
            data=children,
            status=status.HTTP_200_OK
        )


class CreateCourseView(generics.CreateAPIView):
    serializer_class = serializers.CreateCourseSerializer
    permission_classes = (IsAdminUser,)

    def post(self, request):
        course_name = request.data.get('name')
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            data={
                "success": True,
                "message": f"Course {course_name} has been created successfully!"
                },
            status=status.HTTP_201_CREATED
        )


class CreateHomeWorkView(generics.CreateAPIView):
    serializer_class = serializers.CreateHomeWorkSerializer
    permission_classes = (IsAuthenticated,)


    def post(self, request):
        course = request.data["course_id"]
        course_uuid = Course.objects.filter(name=course).first().course_id

        if not request.POST._mutable:
            request.POST._mutable = True
            request.data["course_id"] = course_uuid
            request.POST._mutable = False

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            data={
                "success": True,
                "message": "Ready",
                "result": serializer
            }
        )