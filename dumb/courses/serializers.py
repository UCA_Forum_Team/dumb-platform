from . import models
from users.models import Teacher, Student
from rest_framework import serializers


class CourseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Course
        exclude = ('course_id',)


class CreateCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Course
        fields = '__all__'


class CreateHomeWorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CourseHomeWork
        fields = '__all__'