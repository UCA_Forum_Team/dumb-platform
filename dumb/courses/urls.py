from django.urls import path
from . import views as course_views


app_name = "courses"


urlpatterns = [
    path('all/', 
        course_views.CourseListView.as_view(), name='all_courses'),

    path('teacher_courses/', 
        course_views.TeacherCoursesListView.as_view(), name='teacher_courses'),

    path('student_courses/', 
        course_views.StudentCoursesListView.as_view(), name='student_courses'),

    path('course_teachers/',
        course_views.CourseOwnersList.as_view(), name='course_owners'),

    path('course_students/',
        course_views.CourseStudentsList.as_view(), name='course_students'),
    
    path('create_course/',
        course_views.CreateCourseView.as_view(), name='create_course'),
    
    path('create_homework/',
        course_views.CreateHomeWorkView.as_view(), name='create_homework'),
]
