import uuid
import datetime as dt
from django.db import models
from django.conf import settings
from django.utils import timezone
from users.models import Student, Teacher
from django.utils.translation import ugettext_lazy as _


class Course(models.Model):
    course_id = models.UUIDField(
        verbose_name=_('Course ID'),
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )

    name = models.CharField(
        verbose_name=_('Course Name'),
        max_length=32,
        unique=True,
        db_index=True,
    )

    date_created = models.DateField(
        verbose_name=_('Date Created'),
        default=timezone.now
    )

    is_premium = models.BooleanField(
        verbose_name=_('Premium Course'),
        default=False
    )

    students = models.ManyToManyField(
        Student,
        related_name='students',
        blank=True
    )

    owners = models.ManyToManyField(
        Teacher,
        related_name='owners',
        blank=True
    )

    course_img = models.ImageField(
        verbose_name=_('Course Photo'),
        upload_to='courses_photos/',
        blank=True
    )

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')

    def __str__(self):
        return self.name


class CourseHomeWork(models.Model):
    def __init__(self, *args, **kwargs):
        super(CourseHomeWork, self).__init__(*args, **kwargs)

    course_id = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        related_name='course_uuid'
    )

    student_id = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        related_name='student_name'
    )

    description = models.CharField(
        verbose_name=_('HomeWork Description'),
        max_length=64
    )

    details = models.TextField(
        verbose_name=_('Detailed Explanation'),
        max_length=2048
    )

    date_created = models.DateTimeField(
        verbose_name=_('Created at'),
        default=timezone.now
    )

    date_due_to = models.DateTimeField(
        verbose_name=_('HomeWork Deadline')
    )

    @property
    def time_left(self):
        return self.date_due_to - dt.datetime.now()

    @property
    def deadline(self):
        return self.date_due_to

    def __str__(self):
        return f"{self.course_id} | {self.description}"

# eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ImQ1YzBiYWMzLTk0NDQtNGQ2ZS1hYTdhLWY2Mzk1YWZiMTRmNSIsImV4cCI6MTU5NzQ1NjE4MH0.p5H08-pajZirQmZkkUs8pCk7bnTpWGKW9WfYmrO417U