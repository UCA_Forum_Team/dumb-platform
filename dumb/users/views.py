import json
from django.db import transaction
from django.template import loader
from django.contrib.auth import logout
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from allauth.account.views import PasswordResetView
from authentication.backends import JWTAuthentication
from rest_email_auth.generics import SerializerSaveView
from rest_framework import status, serializers, generics
from .models import CustomUser, Student, Teacher, AdminPost, Mentor

from rest_framework.permissions import (
    IsAuthenticated,
    AllowAny,
    IsAuthenticatedOrReadOnly,
    IsAdminUser
)

from .serializers import (
    LoginSerializer,
    MentorSerializer,
    TeacherSerializer,
    StudentSerializer,
    AdminPostSerializer,
    TeacherListSerializer,
    StudentListSerializer,
    MentorCreateSerializer,
    RegistrationSerializer,
    PasswordChangeSerializer,
    CustomUserListSerializer,
    AdminPostCreateSerializer,
)



class CustomUserDetailView(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CustomUserListSerializer

    def post(self, request):
        serializer = CustomUserListSerializer(request.user)
        return Response(serializer.data)



class CustomUserListView(generics.ListAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CustomUserListSerializer

    def get(self, request):
        queryset = CustomUser.objects.all().values()
        return Response(
            {
                "success": True,
                "result": queryset
            }, status=status.HTTP_200_OK
        )


class CustomUserCreateView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {   "success": True,
                "token": serializer.data.get('token', None),
                "position": serializer.data.get('position', None)
            }, status.HTTP_201_CREATED
        )



class CustomUserDeletelView(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        if 'user_email' in request.data.keys():
            try:
                user = CustomUser.objects.get_by_natural_key(request.data['user_email'])
            except CustomUser.DoesNotExist:
                return Response({
                    "success": True,
                    "message": "User does not exist!",
                    "result": []
                }, status.HTTP_204_NO_CONTENT)
        else:
            user = CustomUser.objects.get_by_natural_key(request.user.email)

        user.delete()
        return Response({
            "success": True,
            "result": f"User {request.data['user_email']} has been Deleted Successfully!"
        }, status.HTTP_205_RESET_CONTENT)



class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )



class LogoutAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        logout(request)
        response = {
            "success": True,
            "result": ("Вы уже уходите? Как жаль... Ну и пошёл(a) тогда нахуй, чёрт!")
        }
        return Response(response, status=status.HTTP_202_ACCEPTED)



class ChangePasswordAPIView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PasswordChangeSerializer

    def put(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data['new_password'])
        request.user.save()
        return Response(
            data={
                "success": True,
                "result": "Password has been changed Successfully!"
            },
            status=status.HTTP_204_NO_CONTENT
        )



class TeacherDetailsView(APIView):
    permission_classes = (IsAdminUser,)
    authentication_classes = (JWTAuthentication,)
    serializer_class = TeacherListSerializer

    def post(self, request):
        try:
            teacher = Teacher.objects.get(user_id=request.user)
            teacher_info = Teacher.objects.filter(username=teacher.username).values()
            return Response(
                teacher_info,
                status=status.HTTP_200_OK
            )
        except Teacher.DoesNotExist:
            return Response(
                data={
                    "success": False,
                    "result": "Teacher does not exist!"
                },
                status=status.HTTP_404_NOT_FOUND
            )



class StudentDetailsView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JWTAuthentication,)
    serializer_class = StudentListSerializer


    def post(self, request):
        try:
            student = Student.objects.get(user_id=request.user)
            student_info = Student.objects.filter(username=student.username).values()
            return Response(
                data={
                    "success": True,
                    "result": student_info[0]
                },
                status=status.HTTP_200_OK
            )
        except Student.DoesNotExist:
            return Response(
                data={
                    "success": False,
                    "result": "Student does not exist!"
                },
                status=status.HTTP_404_NOT_FOUND
            )



class TeacherListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TeacherListSerializer

    def get(self):
        queryset = Teacher.objects.all().values()
        return Response(
            data=queryset,
            status=status.HTTP_200_OK
        )



class StudentListView(generics.ListAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = StudentListSerializer
    queryset = Student.objects.all()



class TeacherCreateView(generics.CreateAPIView):
    permission_classes = (IsAdminUser,)
    serializer_class = TeacherSerializer

    def post(self, request):
        try:
            tk = CustomUser.objects.get(email=request.data["email"]).token
            tch_uuid = CustomUser.objects.get(email=request.data["email"]).user_id

            try:
                Teacher.objects.get(user_id=tch_uuid)
                return Response(
                {   "success": False,
                    "result": "Teacher is already registered.",
                }, status.HTTP_400_BAD_REQUEST
            )

            except:
                if not request.POST._mutable:
                    request.POST._mutable = True
                    request.data["user_id"] = tch_uuid
                    request.POST._mutable = False

                CustomUser.objects.filter(email=request.data["email"]).update(position="teacher")
                serializer = self.serializer_class(data=request.data)
                serializer.is_valid()
                serializer.save()

            return Response(
                {   "success": True,
                    "token": tk,
                }, status.HTTP_201_CREATED
            )

        except:
            return Response(
                {   "success": False,
                    "result": "Email has not been found!",
                }, status.HTTP_400_BAD_REQUEST
            )



class StudentCreateView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = StudentSerializer

    def post(self, request):
        try:
            tk = CustomUser.objects.get(email=request.data["email"]).token
            std_uuid = CustomUser.objects.get(email=request.data["email"]).user_id

            try:
                Student.objects.get(user_id=std_uuid)
                return Response(
                {   "success": False,
                    "result": "Student is already registered.",
                }, status.HTTP_400_BAD_REQUEST
            )

            except:
                if not request.POST._mutable:
                    request.POST._mutable = True
                    request.data["email"] = std_uuid
                    request.POST._mutable = False

                CustomUser.objects.filter(email=request.data["email"]).update(position="student")
                serializer = self.serializer_class(data=request.data)
                serializer.is_valid()
                serializer.save()


                return Response(
                    {   "success": True,
                        "token": tk,
                    }, status.HTTP_201_CREATED
                )

        except CustomUser.DoesNotExist:
            return Response(
                {   "success": False,
                    "result": "Email has not been found!",
                }, status.HTTP_400_BAD_REQUEST
            )



class TeacherUpdateView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        try:
            teacher = Teacher.objects.get(user_id=request.user)
            serializer = TeacherSerializer(teacher, data=request.data, partial=True)
            
            if serializer.is_valid():
                serializer.save()

                return Response(
                    data={
                        "success": True,
                        "messsage": serializer.data,
                    },
                    status=status.HTTP_202_ACCEPTED
                )
            
            return Response(
                    data={
                        "success": False,
                        "error": serializer.errors,
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        except Teacher.DoesNotExist:
            return Response(
                    data={
                        "success": False,
                        "error": "Sorry, such Teacher does not exist!",
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )


class StudentUpdateView(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        try:
            student = Student.objects.get(user_id=request.user)
            serializer = StudentSerializer(student, data=request.data, partial=True)
            
            if serializer.is_valid():
                serializer.save()

                return Response(
                    data={
                        "success": True,
                        "messsage": serializer.data,
                    },
                    status=status.HTTP_202_ACCEPTED
                )
            
            return Response(
                    data={
                        "success": False,
                        "error": serializer.errors,
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        except Student.DoesNotExist:
            return Response(
                    data={
                        "success": False,
                        "error": "Sorry, such Student does not exist!",
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )



class TeacherDestroyView(generics.DestroyAPIView):
    permissions_class = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            teacher = Teacher.objects.get(user_id=request.user.user_id)
            name = teacher
            teacher.delete()

            return Response(
                data={
                    "success": True,
                    "result": f"Teacher {name} has been deleted Successfully!"
                },
                status=status.HTTP_200_OK
            )
        except Teacher.DoesNotExist:
            return Response(
                data={
                    "success": False,
                    "result": f"Cannot be deleted - this Teacher does not exist!"
                },
                status=status.HTTP_400_BAD_REQUEST
            )



class StudentDestroyView(generics.DestroyAPIView):
    permissions_class = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            student = Student.objects.get(user_id=request.user.user_id)
            name = student
            student.delete()

            return Response(
                data={
                    "success": True,
                    "result": f"Student {name} has been deleted Successfully!"
                },
                status=status.HTTP_200_OK
            )
        except Teacher.DoesNotExist:
            return Response(
                data={
                    "success": False,
                    "result": f"Cannot be deleted - this Student does not exist!"
                },
                status=status.HTTP_400_BAD_REQUEST
            )



class MentorCreateView(generics.CreateAPIView):
    permissions_class = (IsAdminUser,)

    def post(self, request):
        serializer = MentorCreateSerializer(data=request.data)

        print()
        print(serializer)
        print(type(serializer))
        print()
        if serializer.is_valid():
            serializer.save()

            return Response(
                data={
                    "success": True,
                    "result": f"Mentor has been created!"
                },
                status=status.HTTP_201_CREATED
            )
        
        return Response(
                data={
                    "success": False,
                    "result": serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )



class MentorListView(generics.ListAPIView):
    permissions_class = (AllowAny,)

    def get(self, request, *args, **kwargs):
        queryset = Mentor.objects.all().values()
        
        return Response(
            data={
                "success": True,
                "result": queryset
            },
            status=status.HTTP_200_OK
        )



class UpdateDestroyView(generics.UpdateAPIView, generics.DestroyAPIView):
    permissions_class = (IsAdminUser)

    def patch(self, request, *args, **kwargs):
        try:
            id = request.data["id"]
        except:
            return Response(
                data={
                    "success": False,
                    "result": "Sorry, such Mentor does not exist, check ID!"
                }
            )

        serializer = MentorSerializer(None, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()

            return Response(
                data={
                    "success": True,
                    "result": serializer.data
                },
                status=status.HTTP_201_CREATED
            )
        
        return Response(
                data={
                    "success": False,
                    "result": serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )


    def delete(self, request, *args, **kwargs):
        try:
            mentor_id = request.data["id"]
        except:
            return Response(
                data={
                    "success": False,
                    "result": "Sorry, such Mentor does not exist, check ID!"
                }
            )

        try:
            mentor = Mentor.objects.get(id=mentor_id)
            name = mentor.username
            mentor.delete()

            return Response(
                    data={
                        "success": True,
                        "result": f"Mentor \"{name}\" has been deleted successfully!"
                    },
                    status=status.HTTP_200_OK
                )
        
        except Mentor.DoesNotExist:
            return Response(
                    data={
                        "success": False,
                        "result": "Sorry, such Mentor does not exist!"
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )



class AdminPostCreateView(generics.CreateAPIView):
    permissions_class = (IsAdminUser,)

    def post(self, request):
        try:
            admin_uuid = CustomUser.objects.get(email=request.data["author"]).user_id

            if not request.POST._mutable:
                request.POST._mutable = True
                request.data["author"] = admin_uuid
                request.POST._mutable = False

            serializer = AdminPostSerializer(data=request.data)

            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(
                data={
                    "success": True,
                    "result": "Post created successfully!",
                },
                status=status.HTTP_201_CREATED
            )
        
        except CustomUser.DoesNotExist:
            return Response(
                    data={
                        "success": False,
                        "error": serializer.errors,
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )



class AdminPostListView(generics.ListAPIView):
    permissions_class = (AllowAny,)

    def get(self, *args, **kwargs):
        posts = AdminPost.objects.all().values()

        return Response(
            data={
                "success": True,
                "result": posts
            }
        )



class AdminPostUpdateView(generics.UpdateAPIView):
    permissions_class = (IsAdminUser,)

    def patch(self, request, *args, **kwargs):
        post = AdminPost.objects.get(id=request.data["id"])
        post_name = post.title
        serializer = AdminPostSerializer(post, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()

            return Response(
                data={
                    "success": True,
                    "messsage": f"Post {post_name} updated successfully!",
                },
                status=status.HTTP_202_ACCEPTED
            )
        
        return Response(
                data={
                    "success": False,
                    "error": serializer.errors,
                },
                status=status.HTTP_400_BAD_REQUEST
            )



class AdminPostDestroyView(generics.DestroyAPIView):
    permissions_class = (IsAdminUser,)

    def delete(self, request, *args, **kwargs):
        post = AdminPost.objects.get(id=request.data["id"])
        post_name = post.title

        return Response(
            data={
                    "success": True,
                    "messsage": f"Post #{post_name}# has been deleted!",
                },
                status=status.HTTP_200_OK
            )