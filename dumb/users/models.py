import jwt
import uuid
from . import managers 
from django.db import models
from datetime import datetime
from datetime import timedelta
from django.conf import settings
from django.utils import timezone
from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin




class CustomUser(AbstractBaseUser, PermissionsMixin):
    user_id = models.UUIDField(
        verbose_name=_('User ID'),
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )

    email = models.EmailField(
        verbose_name=_('Email Adress'),
        max_length=65,
        unique=True,
        db_index=True,
        db_column='admin_email',
    )

    username = models.CharField(
        verbose_name=_('Username'),
        max_length=26,
        db_index=True,
        db_column='username'
    )


    date_joined = models.DateField(verbose_name=_('Date Joined'), default=timezone.now)
    active = models.BooleanField(verbose_name=_('Is_Active'), default=True)
    staff = models.BooleanField(verbose_name=_('Is_Staff'), default=False)
    admin = models.BooleanField(verbose_name=_('Is_Admin'), default=False)
    position = models.SlugField(verbose_name=_('Position'), default='guest')

    objects = managers.UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']


    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
    
    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True
    
    def has_module_perms(self, app_label):
        return True

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    @property
    def token(self):
        """
        Позволяет нам получить токен пользователя, вызвав `user.token` вместо
        `user.generate_jwt_token().
        """
        return self._generate_jwt_token()

    def __str__(self):
        return self.email

    def _generate_jwt_token(self):
        """
        Создает веб-токен JSON, в котором хранится идентификатор
        этого пользователя и срок его действия
        составляет 60 дней в будущем.
        """
        dt = datetime.now() + timedelta(days=60)
        token = jwt.encode({
            'id': f"{self.pk}",
            'exp': dt.utcfromtimestamp(dt.timestamp())
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')


class Student(models.Model):
    def __init__(self, *args, **kwargs):
        super(Student, self).__init__(*args, **kwargs)

    MALE = 'male'
    FEMALE = 'female'

    GENDER_CHOICES = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    ]

    user_id = models.OneToOneField(
        CustomUser,
        verbose_name=_('Student'),
        related_name='student_id',
        on_delete=models.SET_NULL,
        null=True
    )

    username = models.CharField(
        verbose_name=_('Student Username'),
        max_length=32,
        db_index=True,
        db_column='username'
    )

    birth_date = models.DateField(
        verbose_name=_('birth_date'),
        db_column='birth_date'
    )

    gender = models.CharField(
        verbose_name=_('gender'),
        max_length=6, 
        choices=GENDER_CHOICES,
        db_column='gender'
    )

    photo = models.ImageField(
        verbose_name=_('Profile Photo'),
        upload_to='student_profile/',
        blank=True
    )

    country = CountryField(
        verbose_name=_('Country'),
        db_column='country',
        blank=True
    )

    phone_number = models.IntegerField(
        verbose_name=_('phone_number'),
        blank=True,
        db_column='phone_number'
    )

    has_card = models.BooleanField(
        verbose_name=_('Has Debit Card'),
        default=False,
        db_column='has_card'
    )

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')


    def __str__(self):
        return self.username



class Teacher(models.Model):
    def __init__(self, *args, **kwargs):
        super(Teacher, self).__init__(*args, **kwargs)

    ML = 'male'
    FML = 'female'

    GENDER_CHOICES = [
        (ML, 'Male'),
        (FML, 'Female'),
    ]

    ART = 'art'
    SCN = 'science'

    DEPARTMENTS = [
        (SCN, 'Science'),
        (ART, 'Art')
    ]

    NR = 'naryn'
    TK = 'tekeli'
    KHR = 'khorog'

    CAMPUSES = [
        (NR, 'Naryn'),
        (TK, 'Tekeli'),
        (KHR, 'Khorog')
    ]


    user_id = models.OneToOneField(
        CustomUser,
        verbose_name=_('Teacher'),
        related_name='teacher_id',
        on_delete=models.SET_NULL,
        null=True
    )

    username = models.CharField(
        verbose_name=_('Teacher Username'),
        max_length=32,
        db_index=True,
        db_column='username'
    )

    birth_date = models.DateField(
        verbose_name=_('birth_date'),
        db_column='birth_date'
    )

    gender = models.CharField(
        verbose_name=_('gender'),
        max_length=6, 
        choices=GENDER_CHOICES,
        default=ML,
        db_column='gender'
    )

    department = models.CharField(
        verbose_name=_('department'),
        max_length=7,
        choices=DEPARTMENTS,
        default=ART,
        db_column='department'
    )

    campus = models.CharField(
        verbose_name=_('Campus'),
        max_length=6,
        choices=CAMPUSES,
        default=NR,
        db_column='campus'
    )

    photo = models.ImageField(
        verbose_name=_('Profile Photo'),
        upload_to='teacher_profile/',
        blank=True
    )

    phone_number = models.IntegerField(
        verbose_name=_('phone_number'),
        db_column='phone_number',
        blank=True
    )

    country = CountryField(
        verbose_name=_('Country'),
        db_column='country',
        blank=True
    )

    has_card = models.BooleanField(
        verbose_name=_('Has Debit Card'),
        db_column='has_card',
        default=False,
        blank=True
    )

    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')


    def __str__(self):
        return self.username



class Mentor(models.Model):
    def __init__(self, *args, **kwargs):
        super(Mentor, self).__init__(*args, **kwargs)

    ML = 'male'
    FML = 'female'

    GENDER_CHOICES = [
        (ML, 'Male'),
        (FML, 'Female'),
    ]

    NR = 'naryn'
    TK = 'tekeli'
    KHR = 'khorog'

    CAMPUSES = [
        (NR, 'Naryn'),
        (TK, 'Tekeli'),
        (KHR, 'Khorog')
    ]

    mentor = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        related_name='+',
        null=True,
        blank=True
    )

    username = models.CharField(
        verbose_name=_('Mentor Username'),
        max_length=32,
        db_index=True,
        db_column='username'
    )

    birth_date = models.DateField(
        verbose_name=_('birth_date')
    )

    gender = models.CharField(
        verbose_name=_('gender'),
        max_length=6, 
        choices=GENDER_CHOICES,
        default=ML,
        db_column='gender'
    )

    photo = models.ImageField(
        verbose_name=_('Profile Photo'),
        upload_to='mentor_profile/',
        blank=True
    )

    phone_number = models.IntegerField(
        verbose_name=_('phone_number'),
        db_column='phone_number'
    )

    country = CountryField(
        verbose_name=_('Country'),
        db_column='country',
        blank=True
    )

    rewards = models.CharField(
        verbose_name=_('Mentor Rewards'),
        max_length=512,
        blank=True,
    )

    class Meta:
        verbose_name = _('Mentor')
        verbose_name_plural = _('Mentors')


    def __str__(self):
        return self.username



class AdminPost(models.Model):
    def __init__(self, *args, **kwargs):
        super(AdminPost, self).__init__(*args, **kwargs)

    author = models.ForeignKey(
        CustomUser,
        verbose_name=_('Admin Post'),
        on_delete=models.CASCADE,
        related_name=_('admin_post')
    )

    title = models.CharField(
        verbose_name=_('Post Title'),
        max_length=64
    )

    description = models.CharField(
        verbose_name=_('Post Description'),
        max_length=128
    )

    body = models.TextField(
        verbose_name=_('Post Content'),
        max_length=1024
    )

    logo = models.ImageField(
        verbose_name=_('Post Image'),
        blank=True
    )

    date_created = models.DateTimeField(
        verbose_name=_('Post Created'),
        default=timezone.now
    )

    def __str__(self):
        return self.title