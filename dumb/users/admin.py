from django.contrib import admin
from django.contrib.auth.models import Group
from .forms import UserAdminCreationForm, UserAdminChangeForm
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import CustomUser, Student, Teacher, AdminPost, Mentor



class StudentAdmin(admin.ModelAdmin):
    list_display = ('username', 'birth_date', 'gender', 'country', 'has_card')


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('username', 'phone_number', 'department', 'campus', 'country')


class MentorAdmin(admin.ModelAdmin):
    list_display = ('username', 'phone_number', 'rewards', 'country')


class PostAdmin(admin.ModelAdmin):
    list_display = ('author', 'title', 'description', 'body', 'date_created')


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'admin', 'position', 'date_joined', 'token')
    list_filter = ('admin', 'date_joined')
    fieldsets = (
                (None, {'fields': ('email', 'password')}),
                ('Personal info', {'fields': ('username',)}),
                ('Permissions', {'fields': ('admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(Student, StudentAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(CustomUser, UserAdmin)
admin.site.register(AdminPost, PostAdmin)
admin.site.register(Mentor, MentorAdmin)


# Remove Group Model from admin. We're not using it.
admin.site.unregister(Group)
