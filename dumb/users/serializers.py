from . import models
from rest_framework import serializers
from allauth.account.forms import ResetPasswordForm
from django.utils.translation import gettext_lazy as _
from rest_auth.serializers import PasswordResetSerializer
from django_countries.serializers import CountryFieldMixin
from django.contrib.auth import password_validation, authenticate

class CustomUserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = [
            'username',
            'email',
            'date_joined',
            'user_permissions',
            'is_superuser',
            'active',
            'admin',
            'position',
        ]



class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True,
    )

    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = models.CustomUser
        fields = ('user_id', 'username', 'email', 'password', 'token', 'position')
        read_only_fields = ('user_id', 'position')

    def create(self, validated_data):
        return models.CustomUser.objects.create_user(**validated_data)



class LoginSerializer(serializers.Serializer):
    # As the Authencation will be by Email, User has to provide these two mandatory fields.
    email = serializers.CharField(write_only=True)
    password = serializers.CharField(max_length=128, write_only=True)

    # All other fields should be Ignored
    success = serializers.BooleanField(read_only=True)
    position = serializers.CharField(max_length=32, read_only=True)
    username = serializers.CharField(max_length=32, read_only=True)
    token = serializers.CharField(max_length=128, read_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        if email is None:
            raise serializers.ValidationError('Email address is required to log in.')

        if password is None:
            raise serializers.ValidationError('Password address is required to log in.')

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError('User with such Email and Password has not been found!')

        if not user.is_active:
            raise serializers.ValidationError(f'User {user.username} is DEACTIVATED!')

        return {
            "success": True,
            "token": f"{user.token}",
            "position": f"{user.position}"
        }



class PasswordChangeSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_current_password(self, value):
        if not self.context['request'].user.check_password(value):
            raise serializers.ValidationError('Current password does not match')
        return value

    def validate_new_password(self, value):
        password_validation.validate_password(value)
        return value



class TeacherListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Teacher
        exclude = (
            'id',
            'photo',
            'teacher',
            'has_card'
        )



class StudentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Student
        fields = (
            'username',
            'birth_date',
            'gender',
            'country'
        )



class StudentSerializer(CountryFieldMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Student
        fields = "__all__"



class TeacherSerializer(CountryFieldMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Teacher
        fields = "__all__"



class AdminPostCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    logo = serializers.ImageField(read_only=True)
    date_created = serializers.DateTimeField(read_only=True)

    class Meta:
        model = models.AdminPost
        fields = '__all__'

    def create(self, validated_data):
        return models.AdminPost.objects.create(**validated_data)



class MentorCreateSerializer(CountryFieldMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Mentor
        fields = '__all__'

    def create(self, validated_data):
        return models.Mentor.objects.create(**validated_data)



class MentorSerializer(CountryFieldMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Mentor
        fields = '__all__'



class AdminPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AdminPost
        fields = '__all__'

    def create(self, validated_data):
        return models.AdminPost.objects.create(**validated_data)
