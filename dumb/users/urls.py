from django.urls import path
from . import views as users_views
from rest_framework.authtoken import views
from rest_auth.registration.views import VerifyEmailView

app_name = "users"


urlpatterns = [
    path('login/', 
        users_views.LoginAPIView.as_view(), name='account_login'),

    path('logout/', 
        users_views.LogoutAPIView.as_view(), name='account_logout'),

    path('get_user/', 
        users_views.CustomUserDetailView.as_view(), name='account'),

    path('get_users/', 
        users_views.CustomUserListView.as_view(), name='all_accounts'),

    path('register/', 
        users_views.CustomUserCreateView.as_view(), name='register_account'),

    path('delete_account/', 
        users_views.CustomUserDeletelView.as_view(), name='delete_account'),

    path('create_student/', 
        users_views.StudentCreateView.as_view(), name='create_student'),

    path('create_teacher/', 
        users_views.TeacherCreateView.as_view(), name='create_teacher'),

    path('password_change/',
        users_views.ChangePasswordAPIView.as_view(), name='password_change'),

    path('get_teacher/',
        users_views.TeacherDetailsView.as_view(), name='teacher_info'),

    path('get_teachers/',
        users_views.TeacherListView.as_view(), name='all_teachers'),
    
    path('get_student/',
        users_views.StudentDetailsView.as_view(), name='student_info'),

    path('get_students/',
        users_views.StudentListView.as_view(), name='all_students'),

    path('update_teacher/',
        users_views.TeacherUpdateView.as_view(), name='update_teacher'),
    
    path('update_student/',
        users_views.StudentUpdateView.as_view(), name='update_student'),

    path('delete_teacher/',
        users_views.TeacherDestroyView.as_view(), name='delete_teacher'),

    path('delete_student/',
        users_views.StudentDestroyView.as_view(), name='delete_student'),

    path('create_admin_post/',
        users_views.AdminPostCreateView.as_view(), name='create_admin_post'),

    path('update_admin_post/',
        users_views.AdminPostUpdateView.as_view(), name='change_admin_post'),

    path('delete_admin_post/',
        users_views.AdminPostDestroyView.as_view(), name='delete_admin_post'),

    path('getlist_admin_post/',
        users_views.AdminPostListView.as_view(), name='getlist_admin_post'),

    path('create_mentor/',
        users_views.MentorCreateView.as_view(), name='create_mentor'),
    
    path('get_mentors/',
        users_views.MentorListView.as_view(), name='get_mentors'),

    path('update_or_delete_mentor/',
        users_views.UpdateDestroyView.as_view(), name='update_delete_mentor'),
]
